//
//  BezierPathView.swift
//  Dropit
//
//  Created by Yichi Zhang on 9/22/15.
//  Copyright © 2015 Yichi Zhang. All rights reserved.
//

import UIKit

class BezierPathView: UIView {
    
    var bezierPaths = [String: UIBezierPath]()
    
    func setPath(path: UIBezierPath?, named name: String) {
        bezierPaths[name] = path
        setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect) {
        for (_, path) in bezierPaths {
            path.stroke()
        }
    }


}
